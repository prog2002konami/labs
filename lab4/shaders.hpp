#include <string>

const std::string floorVertexShader = R"(
#version 460 core

layout (location = 0) in vec2 position;
layout (location = 1) in vec2 texcoord;
out vec2 vsTexcoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main() 
{
	gl_Position = projection * view * model * vec4(position, 0.0, 1.0);
	vsTexcoord = texcoord;
}	
)";

const std::string floorFragmentShader = R"(
#version 460 core

layout(binding = 0) uniform sampler2D uTexture;

in vec2 vsTexcoord;
out vec4 color;

uniform vec4 chessboardColor;

void main()
{
	color = mix(chessboardColor, texture(uTexture, vsTexcoord), 0.7);
}
)";


const std::string cubeVertexShader = R"(
#version 460 core

layout (location = 0) in vec3 position;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec3 vs_position;

void main() 
{
	gl_Position = projection * view * model * vec4(position, 1.0);
	// non-transformed vertex positions for texture mapping
	vs_position = position;
}
)";

const std::string cubeFragmentShader = R"(
#version 460 core

layout(binding = 1) uniform samplerCube uCubeTexture;

in vec3 vs_position;
vec4 color;
out vec4 finalColor;

uniform vec4 blendColor;

void main()
{
	color = mix(blendColor, texture(uCubeTexture, vs_position), 0.8);
	finalColor = vec4(color.rgb, 0.5);
}
)";

