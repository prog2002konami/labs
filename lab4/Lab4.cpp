#include <iostream>

#include <ShadersDataTypes.h>
#include <VertexArray.h>
#include <Shader.h>
#include <RenderCommands.h>
#include <TextureManager.h>
#include <PerspectiveCamera.h>

#include <GeometricTools.h>

#include <GLFW/glfw3.h>
#include <glm/gtc/matrix_transform.hpp>

#include "Lab4.h"
#include "shaders.hpp"
#include "keyinput.h"

unsigned int Lab4::Run() const
{
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_BLEND);
	// the function is s*apha + d(1-alpha)
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glfwSetKeyCallback(window, keyCallback);
	// for processing arrow input
	bool key = false;

	glm::vec4 white = { 1.0f, 1.0f, 1.0f, 1.0f };
	glm::vec4 black = { 0.0f, 0.0f, 0.0f, 1.0f };
	glm::vec4 purple = { 164.0f / 256.0f, 96.0f / 256.0f, 237.0f / 256.0f, 1.0f };
	glm::vec4 pink = { 241.0f / 256.0f, 91.0f / 256.0f, 181.0f / 256.0f, 1.0f };
	glm::vec4 yellow = { 254.0f / 256.0f, 228.0f / 256.0f, 30.0f / 256.0f, 1.0f };
	glm::vec4 coralblue = { 40.0f / 256.0f, 90.0f / 256.0f, 256.0f / 256.0f, 1.0f };
	glm::vec4 red = { 1.0, 0.0, 0.0, 1.0 };
	glm::vec4 green = { 0.0, 1.0, 0.0, 1.0 };
	glm::vec4 blue = { 0.0, 0.0, 1.0, 1.0 };

	auto floor = GeometricTools::UnitGridGeometry2DWTCoords<8, 8>();
	auto floorTopology = GeometricTools::UnitGrid2DTopologyTriangles<8, 8>();
	auto floorTopologyMesh0 = GeometricTools::UnitGrid2DTopologyMesh0<8, 8>();
	auto floorTopologyMesh1 = GeometricTools::UnitGrid2DTopologyMesh1<8, 8>();

	auto cube = GeometricTools::UnitCube3D;
	auto cubeTopology = GeometricTools::UnitCube3DTopology;

	// create buffers and array for floor
	auto floorVA = std::make_shared<VertexArray>();
	auto floorVB = std::make_shared<VertexBuffer>(floor.data(), sizeof(floor));
	auto floorBufferLayout = BufferLayout({ 
		{ShaderDataType::Float2, "floorPosition"}, 
		{ShaderDataType::Float2, "floorTextureCoords"} });
	floorVB->SetLayout(floorBufferLayout);
	floorVA->AddVertexBuffer(floorVB);
	auto floorIB = std::make_shared<IndexBuffer>(floorTopology.data(), floorTopology.size());
	auto floorIB0 = std::make_shared<IndexBuffer>(floorTopologyMesh0.data(), floorTopologyMesh0.size());
	auto floorIB1 = std::make_shared<IndexBuffer>(floorTopologyMesh1.data(), floorTopologyMesh1.size());

	// create buffers and array for cube
	auto cubeVA = std::make_shared<VertexArray>();
	auto cubeVB = std::make_shared<VertexBuffer>(cube.data(), sizeof(cube));
	auto cubeLayout = BufferLayout({ {ShaderDataType::Float3, "cubePosition"} });
	cubeVB->SetLayout(cubeLayout);
	cubeVA->AddVertexBuffer(cubeVB);
	auto cubeIB = std::make_shared<IndexBuffer>(cubeTopology.data(), cubeTopology.size());
	cubeVA->SetIndexBuffer(cubeIB);

	TextureManager* textures = TextureManager::GetInstance();
	textures->LoadTexture2DRGBA("floorTexture",	
		std::string(TEXTURE_DIR) + std::string("floor.jpg"), 0);
	textures->LoadCubeMapRGBA("cubeTexture", 
		std::string(TEXTURE_DIR) + std::string("pattern.jpg"), 1);
	
	auto floorShader = std::make_shared<Shader>(floorVertexShader.c_str(), floorFragmentShader.c_str());
	auto cubeShader = std::make_shared<Shader>(cubeVertexShader.c_str(), cubeFragmentShader.c_str());
	cubeShader->UploadUniformFloat4("blendColor", white);

	PerspectiveCamera camera({ 45.0f, 1.0f, 1.0f, 0.1f, -10.0f }, glm::vec3(0.0f, 0.0f, 5.0f), 
		glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	floorShader->UploadUniformMatrixFloat4("projection", camera.GetProjectionMatrix());
	cubeShader->UploadUniformMatrixFloat4("projection", camera.GetProjectionMatrix());
	
	glm::mat4 accumulatedRotation(1.0f);

	while (!glfwWindowShouldClose(window))
	{
		float deltax = 0, deltay = 0, deltaz = 0;

		RenderCommands::SetClearColor(purple);
		RenderCommands::Clear();

		// rotate key input
		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) deltax -= 0.05f;
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) deltax += 0.05f;
		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) deltay -= 0.05f;
		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) deltay += 0.05f;
		if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS) deltaz -= 0.05f;
		if (glfwGetKey(window, GLFW_KEY_J) == GLFW_PRESS) deltaz += 0.05f;

		// view matrix: transform to the coordinates that are in front of the user's view
		// lookat function generate a transform from the world space into the specific eye space
		floorShader->UploadUniformMatrixFloat4("view", camera.GetViewMatrix());
		cubeShader->UploadUniformMatrixFloat4("view", camera.GetViewMatrix());

		// floor
		glm::mat4 model(1.0f);
		model = glm::translate(model, glm::vec3(0.0f, -1.0f, -1.0f));
		model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(9.0f, 9.0f, 9.0f));
		floorShader->UploadUniformMatrixFloat4("model", model);
		// cube
		glm::mat4 curRotation(1.0f);
		curRotation = glm::rotate(curRotation, glm::radians(deltax), glm::vec3(1.0f, 0.0f, 0.0f));
		curRotation = glm::rotate(curRotation, glm::radians(deltay), glm::vec3(0.0f, 1.0f, 0.0f));
		curRotation = glm::rotate(curRotation, glm::radians(deltaz), glm::vec3(0.0f, 0.0f, 1.0f));
		accumulatedRotation = curRotation * accumulatedRotation;
		cubeShader->UploadUniformMatrixFloat4("model", accumulatedRotation);

		// draw call
		// floor
		glEnable(GL_DEPTH_TEST);
		floorShader->Bind();
		floorVA->Bind();
		floorShader->UploadUniformFloat4("chessboardColor", white);
		floorVA->SetIndexBuffer(floorIB0);
		RenderCommands::DrawIndex(floorVA, GL_TRIANGLES);
		floorShader->UploadUniformFloat4("chessboardColor", black);
		floorVA->SetIndexBuffer(floorIB1);
		RenderCommands::DrawIndex(floorVA, GL_TRIANGLES);
		// cube
		glDisable(GL_DEPTH_TEST);
		cubeVA->Bind();
		cubeShader->Bind();
		// color blending key input
		if (glfwGetKey(window, GLFW_KEY_0) == GLFW_PRESS)
			cubeShader->UploadUniformFloat4("blendColor", white);
		if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS)
			cubeShader->UploadUniformFloat4("blendColor", red);
		if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS) 
			cubeShader->UploadUniformFloat4("blendColor", green);
		if (glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS)
			cubeShader->UploadUniformFloat4("blendColor", blue);
		RenderCommands::DrawIndex(cubeVA, GL_TRIANGLES);

		glfwPollEvents();
		glfwSwapBuffers(window);
		processInput(window);
	}

	glfwDestroyWindow(window);
	glfwTerminate();

	return EXIT_SUCCESS;
}
