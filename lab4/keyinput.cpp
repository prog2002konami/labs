#include <iostream>
#include "keyinput.h"

void processInput(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
}

void processArrowInput(GLFWwindow* window, std::array<float, 8>& object, bool& key)
{
	float size = (float)1 / 8;
	// right
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS && key == false) {
		key = true;
		if (!(object[0] >= 0.5 - size)) {
			for (int i = 0; i < object.size(); i += 2) {
				object[i] += size;
			}
		}
	}
	// left
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS && key == false) {
		key = true;
		if (!(object[0] <= -0.5)) {
			for (int i = 0; i < object.size(); i += 2) {
				object[i] -= size;
			}
		}
	}
	// up
	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS && key == false) {
		key = true;
		if (!(object[1] >= 0.5)) {
			for (int i = 0; i < object.size(); i += 2) {
				object[i + 1] += size;
			}
		}
	}
	// down
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS && key == false) {
		key = true;
		if (!(object[1] <= -0.5 + size)) {
			for (int i = 0; i < object.size(); i += 2) {
				object[i + 1] -= size;
			}
		}
	}
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_RELEASE
		&& glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_RELEASE
		&& glfwGetKey(window, GLFW_KEY_UP) == GLFW_RELEASE
		&& glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_RELEASE
		&& key == true) {
		key = false;
	}
}

void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)
		std::cout << "space is pressed" << std::endl;
}
