#include <GLFWApplication.h>

class Lab4 : public GLFWApplication {
public:
	Lab4(const std::string& name, const std::string& version)
		: GLFWApplication(name, version) {};
	~Lab4() {};
	// Run function
	unsigned int Run() const override;
	GLuint LoadTexture(const std::string& filepath, GLuint slot) const;
	GLuint LoadCubeMap(const std::string& filepath, GLuint slot) const;
};
