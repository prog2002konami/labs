#include "Lab4.h"

int main(int argc, char** argv) {
	Lab4 application("lab4", "1.0");

	application.ParseArguments(argc, argv);
	application.Init();
	return application.Run();
}
