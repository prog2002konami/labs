#ifndef GEOMETRICTOOLS_H_
#define GEOMETRICTOOLS_H_
#include <array>

namespace GeometricTools
{
	constexpr std::array<float, 3 * 2> UnitTriangle2D = {-0.5f, -0.5f, 0.5f, -0.5f, 0.0f, 0.5f};
	 
	constexpr std::array<float, 4 * 2> UnitSquare2D = {
		-0.5f,  0.5f,
		 0.5f,  0.5f,
		 0.5f, -0.5f,
		-0.5f, -0.5f
	};

	constexpr std::array<unsigned int, 6> UnitSquare2DTopology = {
		0,1,2, 2,3,0 
	};

	// receive division number, return an array of indecies for the grid
	template <int X, int Y> 
	constexpr std::array<float, (X + 1) * (Y + 1) * 2> UnitGrid2D() {
		static_assert(X > 0 && Y > 0, "positive number expected for division number");

		std::array<float, (X + 1) * (Y + 1) * 2> data;

		float gridSizeX = (float)1 / X;
		float gridSizeY = (float)1 / Y;
		for (int x = 0; x < X; x++) {
			for (int y = 0; y < Y; y++) {
				data[((X + 1) * y + x) * 2] = -(float)0.5 + x * gridSizeX;
				data[((X + 1) * y + x) * 2 + 1] = (float)0.5 - y * gridSizeY;
			}
		}
		// make sure the edges are 1
		for (int x = 0; x < X; x++) {
			data[((X + 1) * Y + x) * 2] = -(float)0.5 + x * gridSizeX;
			data[((X + 1) * Y + x) * 2 + 1] = -(float)0.5;
		}
		for (int y = 0; y < Y; y++) {
			data[((X + 1) * y + X) * 2] = (float)0.5;
			data[((X + 1) * y + X) * 2 + 1] = (float)0.5 - y * gridSizeY;
		}
		data[(X + 1) * (Y + 1) * 2 - 2] = (float)0.5;
		data[(X + 1) * (Y + 1) * 2 - 1] = -(float)0.5;
		return data;
	}

	// return indeices that can fill out index buffer (EBO)
	// left bottom and right top triangles
	template <int X, int Y> 
	constexpr std::array<unsigned int, X * Y * 6> UnitGrid2DTopologyTriangles() {
		std::array<unsigned int, X * Y * 6 > data;

		int at = 0;
		for (int y = 0; y < Y; y++) {
			for (int x = 0; x < X; x++) {
				data[at++] = (X + 1) * y + x;
				data[at++] = (X + 1) * (y + 1) + x;
				data[at++] = (X + 1) * (y + 1) + x + 1;

				data[at++] = (X + 1) * y + x;
				data[at++] = (X + 1) * y + x + 1;
				data[at++] = (X + 1) * (y + 1) + x + 1;
			}
		}
		return data;
	}

	template <int X, int Y> 
	constexpr std::array<unsigned int, X * Y * 3> UnitGrid2DTopologyMesh0() {
		// return indecies which fill each other blocks from the topleft of the grid
		std::array<unsigned int, X * Y * 3 > data;
		int at = 0;
		std::array<int, 2> evenodd = { 0, 1 };
		for (int y = 0; y < Y; y++) {
			for (int x = 0; x < X; x += 2) {
				int idx = y % 2;
				data[at++] = evenodd[idx] + (X + 1) * y + x;
				data[at++] = evenodd[idx] + (X + 1) * (y + 1) + x;
				data[at++] = evenodd[idx] + (X + 1) * (y + 1) + x + 1;

				data[at++] = evenodd[idx] + (X + 1) * y + x;
				data[at++] = evenodd[idx] + (X + 1) * y + x + 1;
				data[at++] = evenodd[idx] + (X + 1) * (y + 1) + x + 1;
			}
		}
		return data;
	}
		
	template <int X, int Y> 
	constexpr std::array<unsigned int, X * Y * 3 > UnitGrid2DTopologyMesh1() {
		// return indecies which fill each other blocks from the block next to the topleft of the grid
		std::array<unsigned int, X * Y * 3 > data;
		int at = 0;
		std::array<int, 2> evenodd = { 1, 0 };
		for (int y = 0; y < Y; y++) {
			for (int x = 0; x < X; x += 2) {
				int idx = y % 2;
				data[at++] = evenodd[idx] + (X + 1) * y + x;
				data[at++] = evenodd[idx] + (X + 1) * (y + 1) + x;
				data[at++] = evenodd[idx] + (X + 1) * (y + 1) + x + 1;

				data[at++] = evenodd[idx] + (X + 1) * y + x;
				data[at++] = evenodd[idx] + (X + 1) * y + x + 1;
				data[at++] = evenodd[idx] + (X + 1) * (y + 1) + x + 1;
			}
		}
		return data;
	}

	// generates the geometry of the grid, including texture coordinates
	// the shape of thegenerated data is PPTTPPTTPPTT..., which is two components
	// for position and two componenets for texture coordinates.
	template<unsigned int X, int Y> 
	constexpr std::array<float, (X + 1)* (Y + 1)* (2 + 2)> UnitGridGeometry2DWTCoords()
	{
		static_assert(X > 0 && Y > 0, "positive number expected for division number");

		std::array<float, (X + 1)* (Y + 1)* (2 + 2)> data;

		float gridSizeX = (float)1 / X;
		float gridSizeY = (float)1 / Y;
		for (int x = 0; x < X; x++) {
			for (int y = 0; y < Y; y++) {
				// position
				data[((X + 1) * y + x) * 4] = -(float)0.5 + x * gridSizeX;
				data[((X + 1) * y + x) * 4 + 1] = (float)0.5 - y * gridSizeY;
				// texture
				data[((X + 1) * y + x) * 4 + 2] = x * gridSizeX;
				data[((X + 1) * y + x) * 4 + 3] = (float)1.0 - y * gridSizeY;
			}
		}
		// make sure the edges are 1
		// bottom line
		for (int x = 0; x < X; x++) {
			// position
			data[((X + 1) * Y + x) * 4] = -(float)0.5 + x * gridSizeX;
			data[((X + 1) * Y + x) * 4 + 1] = -(float)0.5;
			// texture
			data[((X + 1) * Y + x) * 4 + 2] = x * gridSizeX;
			data[((X + 1) * Y + x) * 4 + 3] = (float)0.0;
		}
		// right line
		for (int y = 0; y < Y; y++) {
			// position
			data[((X + 1) * y + X) * 4] = (float)0.5;
			data[((X + 1) * y + X) * 4 + 1] = (float)0.5 - y * gridSizeY;
			// texture
			data[((X + 1) * y + X) * 4 + 2] = (float)1.0;
			data[((X + 1) * y + X) * 4 + 3] = (float)1.0 - y * gridSizeY;
		}
		// bottom right point
		// position
		data[(X + 1) * (Y + 1) * 4 - 4] = (float)0.5;
		data[(X + 1) * (Y + 1) * 4 - 3] = -(float)0.5;
		// texture
		data[(X + 1) * (Y + 1) * 4 - 2] = (float)1.0;
		data[(X + 1) * (Y + 1) * 4 - 1] = (float)0.0;
		
		return data;
	}

	constexpr std::array<float, 3 * 8> UnitCube3D = {
		-0.5f, -0.5f, -0.5f,
		 0.5f, -0.5f, -0.5f,
		 0.5f,  0.5f, -0.5f,
		-0.5f,  0.5f, -0.5f,

		-0.5f, -0.5f,  0.5f,
		 0.5f, -0.5f,  0.5f,
		 0.5f,  0.5f,  0.5f,
		-0.5f,  0.5f,  0.5f,
	};

	constexpr std::array<unsigned int, 36> UnitCube3DTopology = {
		2,3,0, 0,1,2,
		5,4,7, 7,6,5,
		6,7,3, 3,2,6,
		1,5,4, 4,0,1,
		1,5,6, 6,2,1,
		4,7,3, 3,0,4
	};

	constexpr std::array<float, 3 * 24 * 2> UnitCube3D24WNormals = {
		-0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
		 0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
		 0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
		-0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,

		-0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
		 0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
		 0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
		-0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,

		-0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
		-0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
		-0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
		-0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,

		 0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
		 0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
		 0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
		 0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,

		-0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
		 0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
		 0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
		-0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,

		-0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
		 0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
		 0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
		-0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
	};

	constexpr std::array<unsigned int, 6 * 3 * 2> UnitCube3DTopologyTriangles24 = {
		0,1,2, 2,3,0,
		4,5,6, 6,7,4,
		8,9,10, 10,11,8,
		12,13,14, 14,15,12,
		16,17,18, 18,19,16,
		20,21,22, 22,23,20,
	};

}

#endif // GEOMETRICTOOLS_H_
