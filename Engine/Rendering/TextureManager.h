#ifndef TEXTUREMANAGER_H_
#define TEXTUREMANAGER_H_

#include <glad/glad.h>
#include <stb_image.h>

#include <string>
#include <vector>

class TextureManager
{
public:
	enum TextureType {Texture2D, Texture3D, CubeMap, SkyBox};

	struct Texture {
		bool mipmap;
		int width, height, bpp;
		std::string name;
		std::string filepath;
		GLuint unit;
		TextureManager::TextureType type;
	};

private:
	std::vector<TextureManager::Texture> Textures;

private:
	static inline TextureManager* Instance = nullptr;

public:
	static TextureManager* GetInstance() {
		return TextureManager::Instance != nullptr ? TextureManager::Instance
			: TextureManager::Instance = new TextureManager();
	}

private:
	TextureManager() {};
	~TextureManager() {};
	// The compiler automatically generates thedefault constructor, 
	// copy constructor, copy-assignment operator, and destructor. 
	// "= delete" means that the compiler will not generate these 
	// special member function. 
	// Here we prevent making unecpected object. 
	// Copy constructor: it is called whenever an object is initialized
	// from another object of the same type. e.g. T a = b; or T a(b); 
	// b is of type T.
	TextureManager(const TextureManager&) = delete;
	// Copy-assignment operator: it is called whenever an object 
	// is appears on the left side of an assignment expression. 
	TextureManager& operator=(const TextureManager&) = delete;

public:
	bool LoadTexture2DRGBA(const std::string& name, const std::string& filepath, 
		GLuint unit, bool mipmap = true);
	bool LoadCubeMapRGBA(const std::string& name, const std::string& filepath, 
		GLuint unit, bool mipmap = true);
	GLuint GetUnitByName(const std::string& name) const;

private: 
	unsigned char* LoadTextureImage(const std::string& filepath, int& height,
		int& width, int& bpp, int format) const;
	void FreeTextureImage(unsigned char* data) const;

};

#endif // TEXTUREMANAGER_H_

