#include "PerspectiveCamera.h"

#include <glm/gtc/matrix_transform.hpp>

PerspectiveCamera::PerspectiveCamera(
	const Frustrum& frustrum,
	const glm::vec3& position,
	const glm::vec3& lookAt,
	const glm::vec3& upVector) 
{
	this->CameraFrustrum = frustrum;
	this->Position = position;
	this->LookAt = lookAt;
	this->UpVector = upVector;
	this->RecalculateMatrix();
}

void PerspectiveCamera::RecalculateMatrix() 
{
	this->ProjectionMatrix = glm::perspective(
		glm::radians(CameraFrustrum.angle),
		CameraFrustrum.width/CameraFrustrum.height,
		CameraFrustrum.near, CameraFrustrum.far);
	
	this->ViewMatrix = glm::lookAt(Position, LookAt, UpVector);
	
	this->ViewProjectionMatrix = ProjectionMatrix * ViewMatrix;
}
