#include "Shader.h"
#include <glm/gtc/type_ptr.hpp>
#include <iostream>

void Shader::CompileShader(GLenum shaderType, const std::string& shaderSrc)
{
	const GLchar* ss = shaderSrc.c_str();
	// check for shader compile errors
	int success;
	char infoLog[512];
	if (shaderType == GL_VERTEX_SHADER) {
		VertexShader = glCreateShader(shaderType);
		glShaderSource(VertexShader, 1, &ss, NULL);
		glCompileShader(VertexShader);
		glGetShaderiv(VertexShader, GL_COMPILE_STATUS, &success);
		if (!success) {
			glGetShaderInfoLog(VertexShader, 512, NULL, infoLog);
			std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
		}
	}
	if (shaderType == GL_FRAGMENT_SHADER) {
		FragmentShader = glCreateShader(shaderType);
		glShaderSource(FragmentShader, 1, &ss, NULL);
		glCompileShader(FragmentShader);
		glGetShaderiv(FragmentShader, GL_COMPILE_STATUS, &success);
		if (!success) {
			glGetShaderInfoLog(FragmentShader, 512, NULL, infoLog);
			std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
		}
	}
}

Shader::Shader(const std::string& vertexShaderSrc, const std::string& fragmentShaderSrc) 
{
	ShaderProgram = glCreateProgram();

	CompileShader(GL_VERTEX_SHADER, vertexShaderSrc);
	CompileShader(GL_FRAGMENT_SHADER, fragmentShaderSrc);

	// create a shader program
	glAttachShader(ShaderProgram, VertexShader);
	glAttachShader(ShaderProgram, FragmentShader);
	glLinkProgram(ShaderProgram);

	glDeleteShader(VertexShader);
	glDeleteShader(FragmentShader);
}

Shader::~Shader()
{
	glDeleteProgram(ShaderProgram);
}

void Shader::Bind() const
{
	glUseProgram(ShaderProgram);
}

void Shader::Unbind() const
{
	glUseProgram(0);
}

void Shader::UploadUniformInt(const std::string& name, const glm::uint& value)
{
	glUseProgram(ShaderProgram);
	glUniform1i(glGetUniformLocation(ShaderProgram, name.c_str()), value);
}

void Shader::UploadUniformFloat1(const std::string& name, const glm::float32& value)
{
	glUseProgram(ShaderProgram);
	glUniform1f(glGetUniformLocation(ShaderProgram, name.c_str()), value);
}

void Shader::UploadUniformFloat2(const std::string& name, const glm::vec2& vector) 
{
	glUseProgram(ShaderProgram);
	glUniform2f(glGetUniformLocation(ShaderProgram, name.c_str()), vector[0], vector[1]);
}

void Shader::UploadUniformFloat3(const std::string& name, const glm::vec3& vector)
{
	glUseProgram(ShaderProgram);
	glUniform3f(glGetUniformLocation(ShaderProgram, name.c_str()), vector[0], vector[1], vector[2]);
}

void Shader::UploadUniformFloat4(const std::string& name, const glm::vec4& vector)
{
	glUseProgram(ShaderProgram);
	glUniform4f(glGetUniformLocation(ShaderProgram, name.c_str()), vector[0], vector[1], vector[2], vector[3]);
}

void Shader::UploadUniformMatrixFloat4(
	const std::string& name, const glm::mat4& vector)
{
	// (location, how many matrices to send, if transpose the matrices, pointer)
	glUseProgram(ShaderProgram);
	glUniformMatrix4fv(glGetUniformLocation(ShaderProgram, name.c_str()), 1, GL_FALSE, glm::value_ptr(vector));
}
