#ifndef CAMERA_H_
#define CAMERA_H_

#include "glm/fwd.hpp"
#include <glm/glm.hpp>

class Camera
{
protected:
	glm::mat4 ProjectionMatrix = glm::mat4(1.0f);
	glm::mat4 ViewMatrix = glm::mat4(1.0f);
	glm::mat4 ViewProjectionMatrix = glm::mat4(1.0f);
	glm::vec3 Position = glm::vec3(1.0f);

public:
	Camera() = default;
	~Camera() = default;

protected:
	Camera(const Camera& camera) {
		this->ProjectionMatrix = camera.ProjectionMatrix;
		this->ViewMatrix = camera.ViewMatrix;
		this->ViewProjectionMatrix = camera.ViewProjectionMatrix;
		this->Position = camera.Position;
	}

public:
	const glm::mat4& GetProjectionMatrix() const {
		return this->ProjectionMatrix;
	}
	const glm::mat4& GetViewMatrix() const {
		return this->ViewMatrix; 
	}
	const glm::mat4& GetViewProjectionMatrix() const {
		return this->ViewProjectionMatrix; 
	}
	const glm::vec3& GetPosition() const {
		return this->Position;
	}
	void SetPosition(const glm::vec3& pos) {
		this->Position = pos;
		this->RecalculateMatrix();
	}

protected:
	virtual void RecalculateMatrix() = 0;

};

#endif // CAMERA_H_
