#include <iostream>
#include "TextureManager.h"

unsigned char* TextureManager::LoadTextureImage(const std::string& filepath,
	int& width, int& height, int& bpp, int format) const 
{
	return stbi_load(filepath.c_str(), &width, &height, &bpp, format);
}

	
bool TextureManager::LoadTexture2DRGBA(const std::string& name,
	const std::string& filepath, GLuint unit, bool mipmap)
{
	// load pixel data from a stored image
	int width, height, bpp;
	auto data = LoadTextureImage(filepath, width, height, bpp, STBI_rgb_alpha);
	if (!data) {
		std::cerr << "Failed to load texture" << std::endl;
		return false;
	}

	// generate the texture
	GLuint tex;
	glGenTextures(1, &tex);
	// associate the texture with a specific texture unit
	glActiveTexture(GL_TEXTURE0 + unit);
	glBindTexture(GL_TEXTURE_2D, tex);

	// transfer the image data to the texture in GPU
	// (target, mipmap level, internal format, width, height, border(always 0), format, type, data)
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, 
		GL_UNSIGNED_BYTE, data);

	if (mipmap) {
		glGenerateMipmap(GL_TEXTURE_2D);
	}

	// set texturing parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	Texture texture;
	texture.mipmap = mipmap;
	texture.width = width;
	texture.height = height;
	texture.name = name;
	texture.filepath = filepath;
	texture.unit = unit;
	texture.type = Texture2D;

	this->Textures.push_back(texture);

	this->FreeTextureImage(data);

	return true;
}

bool TextureManager::LoadCubeMapRGBA(const std::string& name, 
	const std::string& filepath, GLuint unit, bool mipmap) 
{
	// load pixel data from a stored image
	int width, height, bpp;
	auto data = LoadTextureImage(filepath.c_str(), width, height, bpp, STBI_rgb_alpha);
	if (!data) {
		std::cerr << "Failed to load texture" << std::endl;
		return 0;
	}

	// generate the texture
	GLuint tex;
	glGenTextures(1, &tex);
	// associate the texture with a specific texture unit
	glActiveTexture(GL_TEXTURE0 + unit);
	glBindTexture(GL_TEXTURE_CUBE_MAP, tex);

	// transfer the image data to the texture in GPU
	for (unsigned int i = 0; i < 6; i++) {
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA8, 
			width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	}

	if (mipmap) {
		glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
	}

	// set the texturing parameters
	// wrapping
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	// filtering
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	Texture texture;
	texture.mipmap = mipmap;
	texture.width = width;
	texture.height = height;
	texture.name = name;
	texture.filepath = filepath;
	texture.unit = unit;
	texture.type = CubeMap;

	this->Textures.push_back(texture);

	this->FreeTextureImage(data);

	return true;

}

GLuint TextureManager::GetUnitByName(const std::string& name) const
{
	for (const auto& texture : this->Textures) {
		// return 0 when they are equal
		if (texture.name.compare(name)) {
			return texture.unit;
		}
	}
	return -1;
}

void TextureManager::FreeTextureImage(unsigned char* data) const
{	
	// free the image data stored in CPU memory (not used anymore)
	stbi_image_free(data);
}

