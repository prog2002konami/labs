#include "OrthographicCamera.h"
#include "glm/gtc/matrix_transform.hpp"
	
OrthographicCamera::OrthographicCamera(
	const Frustrum& frustrum, const glm::vec3& position, float rotation)
{
	this->Rotation = rotation;
	this->CameraFrustrum = frustrum;
	this->Position = position;
	this->RecalculateMatrix();
}

void OrthographicCamera::RecalculateMatrix()
{
	this->ProjectionMatrix = glm::ortho(CameraFrustrum.left, CameraFrustrum.right,
		CameraFrustrum.bottom, CameraFrustrum.top, CameraFrustrum.near, CameraFrustrum.far);
	ViewMatrix = glm::rotate(glm::mat4(1.0f), glm::radians(Rotation), glm::vec3(0.0f, 0.0f, 1.0f));
	this->ViewMatrix = glm::translate(ViewMatrix, -glm::vec3(Position));
	this->ViewProjectionMatrix = ProjectionMatrix * ViewMatrix;
}
