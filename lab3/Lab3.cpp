#include <iostream>

#include <ShadersDataTypes.h>
#include <VertexArray.h>
#include <Shader.h>
#include <RenderCommands.h>

#include <GeometricTools.h>

#include <GLFW/glfw3.h>
#include <glm/gtc/matrix_transform.hpp>

#include "Lab3.h"
#include "shaders.hpp"
#include "keyinput.h"

Lab3::Lab3(const std::string& name_, const std::string& version_)
	: GLFWApplication(name_, version_) {}

Lab3::~Lab3() {}

unsigned int Lab3::Run() const
{
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glfwSetKeyCallback(window, keyCallback);
	// for processing arrow input
	bool key = false;

	glm::vec4 white = { 1.0f, 1.0f, 1.0f, 1.0f };
	glm::vec4 black = { 0.0f, 0.0f, 0.0f, 1.0f };
	glm::vec4 purple = { 164.0f / 256.0f, 96.0f / 256.0f, 237.0f / 256.0f, 1.0f };
	glm::vec4 pink = { 241.0f / 256.0f, 91.0f / 256.0f, 181.0f / 256.0f, 1.0f };
	glm::vec4 yellow = { 254.0f / 256.0f, 228.0f / 256.0f, 30.0f / 256.0f, 1.0f };
	glm::vec4 blue = { 40.0f / 256.0f, 90.0f / 256.0f, 256.0f / 256.0f, 1.0f };
	glm::vec4 green = { 0.0, 1.0, 0.0, 1.0 };

	auto grid = GeometricTools::UnitGrid2D<8, 8>();
	auto mesh0 = GeometricTools::UnitGrid2DTopologyMesh0<8, 8>();
	auto mesh1 = GeometricTools::UnitGrid2DTopologyMesh1<8, 8>();

	auto selector = GeometricTools::UnitSquare2D;
	auto selectorTopology = GeometricTools::UnitSquare2DTopology;

	auto cube = GeometricTools::UnitCube3D;
	auto cubeTopology = GeometricTools::UnitCube3DTopology;

	// create buffers and array for grid
	auto gridVA = std::make_shared<VertexArray>();
	auto gridVB = std::make_shared<VertexBuffer>(grid.data(), sizeof(grid));
	auto gridBufferLayout = BufferLayout({ {ShaderDataType::Float2, "gridPosition"} });
	gridVB->SetLayout(gridBufferLayout);
	gridVA->AddVertexBuffer(gridVB);
	auto gridIB0 = std::make_shared<IndexBuffer>(mesh0.data(), mesh0.size());
	auto gridIB1 = std::make_shared<IndexBuffer>(mesh1.data(), mesh1.size());

	// create buffers and array for selector
	for (int i = 0; i < selector.size(); i++)
		selector[i] /= (float)8;
	float gap_to_top_left = selector[0] - (-0.5);
	for (int i = 0; i < selector.size(); i += 2) {
		selector[i] -= gap_to_top_left;
		selector[i + 1] += gap_to_top_left;
	}
	auto selectorVA = std::make_shared<VertexArray>();
	auto selectorVB = std::make_shared<VertexBuffer>(selector.data(), sizeof(selector));
	auto selectorLayout = BufferLayout({ {ShaderDataType::Float2, "selectorPosition"} });
	selectorVB->SetLayout(selectorLayout);
	selectorVA->AddVertexBuffer(selectorVB);
	auto selectorIB = std::make_shared<IndexBuffer>(&selectorTopology, sizeof(selectorTopology)/4);
	selectorVA->SetIndexBuffer(selectorIB);

	// create buffers and array for cube
	auto cubeVA = std::make_shared<VertexArray>();
	auto cubeVB = std::make_shared<VertexBuffer>(cube.data(), sizeof(cube));
	auto cubeLayout = BufferLayout({ {ShaderDataType::Float3, "cubePosition"} });
	cubeVB->SetLayout(cubeLayout);
	cubeVA->AddVertexBuffer(cubeVB);
	auto cubeIB = std::make_shared<IndexBuffer>(cubeTopology.data(), cubeTopology.size());
	cubeVA->SetIndexBuffer(cubeIB);

	auto chessboardShader = std::make_shared<Shader>(chessboardVertexShader.c_str(), chessboardFragmentShader.c_str());
	auto cubeShader = std::make_shared<Shader>(cubeVertexShader.c_str(), cubeFragmentShader.c_str());

	// projection matrix: determine which vertices will end up on the screen
	// it rarely change so set it only once outside the render loop.
	// note that positive axis goint through your screen towards you
	/* the thirdand fourth parameters separately refer to nearand far plane.
		it looks not the same as z-axis value. it works if I set the third 
		to a positive and small (less than 1) value and the forth to a big value 
		I guess it also depends on the camera */
	glm::mat4 projection(1.0f);
	projection = glm::perspective(glm::radians(45.0f), 600.0f / 600.0f, 0.1f, -10.0f);
	chessboardShader->Bind();
	chessboardShader->UploadUniformMatrixFloat4("projection", projection);
	cubeShader->Bind();
	cubeShader->UploadUniformMatrixFloat4("projection", projection);

	glm::mat4 accumulatedRotation(1.0f);

	while (!glfwWindowShouldClose(window))
	{
		float deltax = 0, deltay = 0, deltaz = 0;

		RenderCommands::SetClearColor(glm::vec4(164.0f / 256.0f, 96.0f / 256.0f, 237.0f / 256.0f, 1.0f));
		RenderCommands::Clear();

		// rotate key input
		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) deltax -= 0.07f;
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) deltax += 0.07f;
		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) deltay -= 0.07f;
		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) deltay += 0.07f;
		if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS) deltaz -= 0.07f;
		if (glfwGetKey(window, GLFW_KEY_J) == GLFW_PRESS) deltaz += 0.07f;

		// view matrix: transform to the coordinates that are in front of the user's view
		// lookat function generate a transform from the world space into the specific eye space
		// same result as view = glm::translate(view, glm::vec3(0, 0, -6)); 
		glm::mat4 view = glm::mat4(1.0f);
		view = glm::lookAt(glm::vec3(0, 0, 5), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
		chessboardShader->Bind();
		chessboardShader->UploadUniformMatrixFloat4("view", view);
		cubeShader->Bind();
		cubeShader->UploadUniformMatrixFloat4("view", view);

		// model matrix: transform the coordinates from local space to world space
		// chessboard
		glm::mat4 model(1.0f);
		model = glm::translate(model, glm::vec3(0.0f, -1.0f, 0.0f));
		model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(8.0f, 8.0f, 8.0f));
		chessboardShader->Bind();
		chessboardShader->UploadUniformMatrixFloat4("model", model);
		// cube
		glm::mat4 curRotation(1.0f);
		curRotation = glm::rotate(curRotation, glm::radians(deltax), glm::vec3(1.0f, 0.0f, 0.0f));
		curRotation = glm::rotate(curRotation, glm::radians(deltay), glm::vec3(0.0f, 1.0f, 0.0f));
		curRotation = glm::rotate(curRotation, glm::radians(deltaz), glm::vec3(0.0f, 0.0f, 1.0f));
		accumulatedRotation = curRotation * accumulatedRotation;
		cubeShader->Bind();
		cubeShader->UploadUniformMatrixFloat4("model", accumulatedRotation);

		// draw call

		gridVA->Bind();
		chessboardShader->Bind();
		RenderCommands::SetSolidMode();
		// chessboard black
		chessboardShader->UploadUniformFloat4("fragColor", black);
		gridVA->SetIndexBuffer(gridIB0);
		RenderCommands::DrawIndex(gridVA, GL_TRIANGLES);
		// chessboard white
		chessboardShader->UploadUniformFloat4("fragColor", white);
		gridVA->SetIndexBuffer(gridIB1);
		RenderCommands::DrawIndex(gridVA, GL_TRIANGLES);
		// selector green
		processArrowInput(window, selector, key);
		selectorVB->Bind();
		selectorVB->BufferSubData(0, 32, &selector);
		selectorVA->Bind();
		chessboardShader->UploadUniformFloat4("fragColor", green);
		RenderCommands::DrawIndex(selectorVA, GL_TRIANGLES);
		// cube
		cubeVA->Bind();
		cubeShader->Bind();
		// first time drawing with filled mode
		cubeShader->UploadUniformFloat4("fragColor", blue);
		RenderCommands::DrawIndex(cubeVA, GL_TRIANGLES);
		// second time drawing with wireframe mode
		RenderCommands::SetWireframeMode();
		cubeShader->UploadUniformFloat4("fragColor", pink);
		RenderCommands::DrawIndex(cubeVA, GL_TRIANGLES);

		glfwPollEvents();
		glfwSwapBuffers(window);
		processInput(window);
	}
	
	glfwDestroyWindow(window);
	glfwTerminate();

	return EXIT_SUCCESS;
}
