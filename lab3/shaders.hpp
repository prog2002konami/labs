#include <string>

const std::string chessboardVertexShader = R"(
#version 460 core

layout (location = 0) in vec2 position;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main() 
{
	gl_Position = projection * view * model * vec4(position, 0.0, 1.0);
}	
)";

const std::string chessboardFragmentShader = R"(
#version 460 core

out vec4 color;
uniform vec4 fragColor;

void main()
{
	color = fragColor;
}
)";

const std::string cubeVertexShader = R"(
#version 460 core

layout (location = 0) in vec3 position;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main() 
{
	gl_Position = projection * view * model * vec4(position, 1.0);
}	
)";

const std::string cubeFragmentShader = R"(
#version 460 core

out vec4 color;
uniform vec4 fragColor;

void main()
{
	color = fragColor;
}
)";

