#include <GLFWApplication.h>

class Lab3 : public GLFWApplication {
public:
	Lab3(const std::string& name, const std::string& version);
	~Lab3();
	// Run function
	unsigned int Run() const override;
};
