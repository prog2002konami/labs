#include "Lab3.h"

int main(int argc, char** argv) {
	Lab3 application("lab3", "1.0");

	application.ParseArguments(argc, argv);
	application.Init();
	return application.Run();
}
