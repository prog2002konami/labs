#include <iostream>
#include <tclap/CmdLine.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <fstream>
#include <sstream>
#include "Lab2Chessboard.h"
#include "chessboardShader.hpp"
#include <GeometricTools.h>
#include <VertexBuffer.h>
#include <IndexBuffer.h>
#include <ShadersDataTypes.h>
#include <BufferLayout.h>
#include <VertexArray.h>
#include <Shader.h>

Lab2Chessboard::Lab2Chessboard(const std::string& name_, const std::string& version_) 
	: GLFWApplication(name_, version_) {}

Lab2Chessboard::~Lab2Chessboard() {}

struct ShaderProgramSource
{
	std::string VertexSource;
	std::string FragmentSource;
};

static ShaderProgramSource ParseShader(const std::string& filepath) 
{
	std::ifstream stream(filepath);

	enum class ShaderType {NONE=-1, VERTEX=0, FRAGMENT=1};

	std::string line;
	std::stringstream ss[2];
	ShaderType type = ShaderType::NONE;
	while (getline(stream, line)) {
		if (line.find("#shader") != std::string::npos) {
			if (line.find("vertex") != std::string::npos) {
				type = ShaderType::VERTEX;
			}
			else if (line.find("fragment") != std::string::npos) {
				type = ShaderType::FRAGMENT;
			}
		}
		else {
			ss[(int)type] << line << "\n";
		}
	}

	return {ss[0].str(), ss[1].str()};
}

static void processInput(GLFWwindow* window) 
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
}

template <int X> static void processArrowInput(GLFWwindow* window, std::array<float,X>& object, bool& key)
{
	float size = (float)1 / 8;
	// right
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS && key == false) {
		key = true;
		if (!(object[0] >= 0.5 - size)) {
			for (int i = 0; i < object.size(); i += 2) {
				object[i] += size;
			}
		}
	}
	// left
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS && key == false) {
		key = true;
		if (!(object[0] <= -0.5)) {
			for (int i = 0; i < object.size(); i += 2) {
				object[i] -= size;
			}
		}
	}
	// up
	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS && key == false) {
		key = true;
		if (!(object[1] >= 0.5)) {
			for (int i = 0; i < object.size(); i += 2) {
				object[i + 1] += size;
			}
		}
	}
	// down
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS && key == false) {
		key = true;
		if (!(object[1] <= -0.5 + size)) {
			for (int i = 0; i < object.size(); i += 2) {
				object[i + 1] -= size;
			}
		}
	}
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_RELEASE 
		&& glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_RELEASE
		&& glfwGetKey(window, GLFW_KEY_UP) == GLFW_RELEASE
		&& glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_RELEASE
		&& key == true) {
		key = false;
	}
}

static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)
		std::cout << "space is pressed" << std::endl;
}

unsigned int Lab2Chessboard::Run() const
{
	glfwSetKeyCallback(window, keyCallback);
	auto grid = GeometricTools::UnitGrid2D<8, 8>();
	auto mesh0 = GeometricTools::UnitGrid2DTopologyMesh0<8, 8>();
	auto mesh1 = GeometricTools::UnitGrid2DTopologyMesh1<8, 8>();

	// create buffers and array for grid
	auto gridVAO = std::make_shared<VertexArray>();
	auto gridVBO = std::make_shared<VertexBuffer>(grid.data(), sizeof(grid));
	auto gridBufferLayout = BufferLayout({ {ShaderDataType::Float2, "gridPosition"} });
	gridVBO->SetLayout(gridBufferLayout);
	gridVAO->AddVertexBuffer(gridVBO);
	auto gridEBO0 = std::make_shared<IndexBuffer>(mesh0.data(), mesh0.size());
	auto gridEBO1 = std::make_shared<IndexBuffer>(mesh1.data(), mesh1.size());

	auto selector = GeometricTools::UnitSquare2D;
	auto selectorTopology = GeometricTools::UnitSquare2DTopology;

	auto selectorVAO = std::make_shared<VertexArray>();
	for (int i = 0; i < selector.size(); i++)
		selector[i] /= (float)8;
	float gap_to_top_left = selector[0] - (-0.5);
	for (int i = 0; i < selector.size(); i += 2) {
		selector[i] -= gap_to_top_left;
		selector[i + 1] += gap_to_top_left;
	}
	auto selectorVBO = std::make_shared<VertexBuffer>(selector.data(), sizeof(selector));
	auto selectorLayout = BufferLayout({ {ShaderDataType::Float2, "squarePosition"} });
	selectorVBO->SetLayout(selectorLayout);
	selectorVAO->AddVertexBuffer(selectorVBO);
	auto selectorEBO = std::make_shared<IndexBuffer>(selectorTopology.data(), selectorTopology.size());
	selectorVAO->SetIndexBuffer(selectorEBO);

	auto shader = std::make_shared<Shader>(chessboardVertexShader.c_str(), chessboardFragmentShader.c_str());

	// for processing arrow input
	bool key = false;

	while (!glfwWindowShouldClose(window))
	{
		glClearColor(164.0f / 256.0f, 96.0f / 256.0f, 237.0f / 256.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		glm::vec4 black = { 0.0, 0.0, 0.0, 0.0 };
		glm::vec4 white = { 1.0, 1.0, 1.0, 1.0 };
		glm::vec4 green = { 0.0, 1.0, 0.0, 1.0 };
		shader->Bind();
		gridVAO->Bind();
		// chessboard black
		shader->UploadUniformFloat4("fragColor", black);
		gridVAO->SetIndexBuffer(gridEBO0);
		glDrawElements(GL_TRIANGLES, 64 * 3, GL_UNSIGNED_INT, 0);
		// chessboard white
		shader->UploadUniformFloat4("fragColor", white);
		gridVAO->SetIndexBuffer(gridEBO1);
		glDrawElements(GL_TRIANGLES, 64 * 3, GL_UNSIGNED_INT, 0);
		// selector green
		shader->UploadUniformFloat4("fragColor", green);
		selectorVAO->Bind();
		processArrowInput(window, selector, key);
		selectorVBO->BufferSubData(0, 32, selector.data());
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

		glfwPollEvents();
		glfwSwapBuffers(window);
		processInput(window);
	}
	
	glfwDestroyWindow(window);
	glfwTerminate();

	return EXIT_SUCCESS;
}
