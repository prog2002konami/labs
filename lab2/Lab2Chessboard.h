#include <GLFWApplication.h>

class Lab2Chessboard : public GLFWApplication {
public:
	Lab2Chessboard(const std::string& name, const std::string& version);
	~Lab2Chessboard();
	// Run function
	unsigned int Run() const override;
};
