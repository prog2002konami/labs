#include "Lab2Chessboard.h"

int main(int argc, char** argv) {
	Lab2Chessboard application("lab2Chessboard", "1.0");

	application.ParseArguments(argc, argv);
	application.Init();
	return application.Run();
}
