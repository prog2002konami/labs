#include "Lab5.h"

int main(int argc, char** argv) {
	Lab5 application("lab5", "1.0");

	application.ParseArguments(argc, argv);
	application.Init();
	return application.Run();
}
