#include <string>

const std::string floorVertexShader = R"(
#version 460 core

layout (location = 0) in vec2 position;
layout (location = 1) in vec2 texcoord;
out vec2 vsTexcoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main() 
{
	gl_Position = projection * view * model * vec4(position, 0.0, 1.0);
	vsTexcoord = texcoord;
}	
)";

const std::string floorFragmentShader = R"(
#version 460 core

layout(binding = 0) uniform sampler2D uTexture;

in vec2 vsTexcoord;
out vec4 color;

uniform vec4 chessboardColor;
uniform float u_ambientStrength = 1.0;

void main()
{
	color = u_ambientStrength*mix(chessboardColor, texture(uTexture, vsTexcoord), 0.7);
}
)";


const std::string cubeVertexShader = R"(
#version 460 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec3 vsPosition;
out vec3 fragNormal;
out vec3 fragPosition;

void main() 
{
	gl_Position = projection * view * model * vec4(position, 1.0);
	// non-transformed vertex positions for texture mapping
	vsPosition = position;
	fragPosition = vec3(model * vec4(position, 1.0));
	fragNormal = normalize(model * vec4(normal, 0.0)).xyz;
}
)";

const std::string cubeFragmentShader = R"(
#version 460 core

layout(binding = 1) uniform samplerCube uCubeTexture;

uniform vec4 blendColor;
uniform float ambientStrength = 0.3;
uniform vec3 lightPosition;
uniform vec3 lightColor = vec3(1.0, 1.0, 1.0);
uniform vec3 cameraPosition;

in vec3 vsPosition;
in vec3 fragNormal;
in vec3 fragPosition;

out vec4 finalColor;

void main()
{
	// ambient
	vec3 ambient = ambientStrength * lightColor;

	// diffuse
	float diffuseStrength = 0.7;
	vec3 lightDir = normalize(lightPosition - fragPosition);
	// degree between fragNormal and lightDir
	float diff = max(dot(fragNormal, lightDir), 0.0);
	vec3 diffuse = diffuseStrength * diff * lightColor;

	// specular
	float specularStrength = 0.1;
	vec3 viewDir = normalize(cameraPosition - fragPosition);
	vec3 reflectDir = reflect(-lightDir, fragNormal);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0f), 64);
	vec3 specular = specularStrength * spec * lightColor;

	vec4 objectColor = mix(blendColor, texture(uCubeTexture, vsPosition), 0.8);

	vec3 result = (ambient + diffuse + specular) * objectColor.rgb;
	//vec3 result = (ambient) * objectColor.rgb;
	//vec3 result = (diffuse) * objectColor.rgb;
	//vec3 result = (specular) * objectColor.rgb;
	finalColor = vec4(result, 1.0);
}
)";

