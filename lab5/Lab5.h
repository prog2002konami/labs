#include <GLFWApplication.h>

class Lab5 : public GLFWApplication {
public:
	Lab5(const std::string& name, const std::string& version)
		: GLFWApplication(name, version) {};
	~Lab5() {};
	// Run function
	unsigned int Run() const override;
};
