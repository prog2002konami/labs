#include <GLFW/glfw3.h>
#include <array>

void processInput(GLFWwindow* window);
void processArrowInput(GLFWwindow* window, std::array<float, 8>& object, bool& key);
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
